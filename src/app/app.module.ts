import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CalendarModule } from 'angular-calendar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CookieModule } from 'ngx-cookie';
import { TextMaskModule } from 'angular2-text-mask';

import { RoutingModule } from './routing/routing.module';
import { CheckRouteGuard } from './check-route/check-route.guard';

import { AppSettings } from './app.settings';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { UserAuthService } from './user-auth/user-auth.service';
import { CustomRequestOptionsService } from './custom-request-options/custom-request-options.service';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { UserService } from './user/user.service';
import { ResetComponent } from './reset/reset.component';
import { PasswordRestoreComponent } from './password-restore/password-restore.component';
import { CalendarComponent } from './calendar/calendar.component';
import { MembershipComponent } from './membership/membership.component';
import { ConfirmComponent } from './confirm/confirm.component';

import { EqualValidator } from './equal-validator.directive';
import { ActionsPipe } from './calendar/actions.pipe';
import { DateActionsPipe } from './calendar/date-actions.pipe';
import { ActionsIdPipe } from './calendar/actions-id.pipe';
import { FilledDaysPipe } from './calendar/filled-days.pipe';

import { SmoothScrollToDirective, SmoothScrollDirective } from 'ng2-smooth-scroll';
import { PaymentFormComponent } from './payment-form/payment-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    ResetComponent,
    PasswordRestoreComponent,
    CalendarComponent,
    MembershipComponent,
    ConfirmComponent,
    EqualValidator,
    ActionsPipe,
    DateActionsPipe,
    ActionsIdPipe,
    FilledDaysPipe,
    SmoothScrollToDirective,
    SmoothScrollDirective,
    PaymentFormComponent,
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RoutingModule,
    CookieModule.forRoot(),
    BrowserAnimationsModule,
    CalendarModule.forRoot(),
    TextMaskModule,
  ],
  providers: [
    UserAuthService,
    CustomRequestOptionsService,
    AppSettings,
    UserService,
    CheckRouteGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
