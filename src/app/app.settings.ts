/**
 * Created by hazarin on 24.05.17.
 */
import { Injectable } from '@angular/core';

@Injectable()
export class AppSettings {

  config = {
    backend: {
      reset_key_length: 8,
      activation_key_length: 8,
      url: 'http://localhost:3000',
      // url: 'http://api.adayclock.com',
      login_uri: '/api/front/login',
      logout_uri: '/api/front/logout',
      profile_uri: '/api/front',
      reset_uri: '/api/front/reset',
      password_uri: '/api/front/password',
      contact_uri: '/api/front/contact',
      membership_uri: '/api/front/membership',
      register_uri: '/api/front/register',
      confirm_uri: '/api/front/confirm',
      calendars_uri: '/api/front/calendars',
      activities_uri: '/api/front/activities',
      products_uri: '/api/front/products',
      order_uri: '/api/front/order',
    }
  };
  loggedIn = false;
  user = null;
  token = null;

  constructor() {
    this.loggedIn = localStorage.getItem('loggedIn') ? JSON.parse(localStorage.getItem('loggedIn')) : false;
    this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
    this.token = localStorage.getItem('token') ? JSON.parse(localStorage.getItem('token')) : null;
  }
}
