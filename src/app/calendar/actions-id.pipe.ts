import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'actionsId'
})

export class ActionsIdPipe implements PipeTransform {

  transform(allDates, id) {
    return allDates.filter(item => item.id !== id);
  }
}
