import { Pipe, PipeTransform } from '@angular/core';
import { CalendarMonthViewDay } from 'angular-calendar';

@Pipe({
  name: 'dateActions'
})

export class DateActionsPipe implements PipeTransform {

  transform(allDates, day:CalendarMonthViewDay) {
    if (day === undefined) {
      return [];
    };
    const now = day.date;
    const today = new Date(Date.UTC(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0));

    return allDates.filter(item => {
      const prom = new Date(item.dt);
      return item.id && +prom === +today;
    });
  }
}
