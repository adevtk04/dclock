/**
 * Created by hazarin on 29.05.17.
 */
import {CalendarDateFormatter, DateFormatterParams} from 'angular-calendar';

export class DateFormatter extends CalendarDateFormatter {

  public monthViewColumnHeader ({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale, {weekday: 'short'}).format(date).substr(0,2).toUpperCase();
  }

  public monthViewTitle ({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale,
      {year: 'numeric', month: 'long'}).format(date);
  }

  public weekViewColumnHeader ({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale, {weekday: 'narrow'}).format(date);
  }

  public dayViewTitle ({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale,
      {weekday: 'long', day: 'numeric', month: 'long'}).format(date);
  }

  public dayViewHour ({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale,
      {hour: 'numeric', minute: 'numeric'}).format(date);
  }

}