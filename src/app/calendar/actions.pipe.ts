import { Pipe, PipeTransform, Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Pipe({
  name: 'actions'
})

export class ActionsPipe implements PipeTransform {

  transform(allDates) {
    return allDates.filter(item => item.id);
  }
}
