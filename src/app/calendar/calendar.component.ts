import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {CalendarMonthViewComponent} from 'angular-calendar';
import {
  CalendarEvent,
  CalendarDateFormatter,
  CalendarMonthViewDay
} from 'angular-calendar';
import {DateFormatter} from './date-formatter';

import {UserService} from '../user/user.service';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {ActionsIdPipe} from "./actions-id.pipe";
import {FilledDaysPipe} from "./filled-days.pipe";
import {startWith} from "rxjs/operator/startWith";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: DateFormatter
    }],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarComponent implements OnInit {

  locale = 'en-US';
  editMode: boolean = false;
  view: string = 'month';
  refresh: Subject<any> = new Subject();
  @Input() monthListTitle: string;

  currentDate: CalendarMonthViewDay;
  viewDate: Date = new Date();
  hMask = [/[0-2]/, /[0-9]/];
  mMask = [/[0-5]/, /[0-9]/];

  unique = [];
  events: CalendarEvent[] = [];

  calendars = [];
  currentCalendar = {title: ''};
  calendarActivities = [];
  @Input() dateActivities = [];

  activity = {
    hour: this.viewDate.getHours().toString(),
    minute: this.viewDate.getMinutes().toString(),
    repeat: 'not',
    activity_date: this.viewDate,
    activity_day: this.viewDate.getDay(),
    activity_time: this.viewDate,
    activity_type: 'medicine',
    isActive: true,
    description: '',
    dt: new Date(Date.UTC(this.viewDate.getFullYear(), this.viewDate.getMonth(), this.viewDate.getDate(), 0, 0, 0)).toISOString(),
  };

  activityTypes = [
    'medicine',
    'visitor',
    'doctor',
    'food',
    'bathe',
    'sleep',
    'other'];

  activeDayIsOpen: boolean = true;

  constructor(
      private userService: UserService) {
  }

  ngOnInit() {
    let self = this;
    this.viewDate.setDate(this.viewDate.getDate() + 1);
    this.setMonthListTitle(this.viewDate);
    this.userService.getCalendars()
        .then((response) => {
          self.calendars = response;
          self.currentCalendar = self.calendars[0];
          // self.getDateActivities(this.viewDate);
          self.getCalendarActivities(this.viewDate);
        })
        .catch((err) => {
          return err;
        });
  }

  setMonthListTitle(date) {
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    let strFirst = new Intl.DateTimeFormat('en-US',
        {year: 'numeric', day: '2-digit', month: '2-digit'}).format(firstDay);
    let strLast = new Intl.DateTimeFormat('en-US',
        {year: 'numeric', day: '2-digit', month: '2-digit'}).format(lastDay);

    this.monthListTitle = 'Actitvities from ' + strFirst + ' till ' + strLast;
  }

  fillCalendarEvents(activities) {
    this.events = [];
    activities.forEach(item => {
      let aDate = new Date(item.dt.replace(' ', 'T'));
      this.events.push({
        start: aDate,
        end: aDate,
        title: '',
        color: {primary: '#ad2121', secondary: '#FAE3E3'},
      });
    });
    this.refresh.next();
  }

  onDelete(item) {
    let self = this;
    if (item.id !== null) {
      this.userService.deleteActivity(this.currentCalendar, item)
          .then(response => {
            let rmList = new ActionsIdPipe();
            self.calendarActivities = rmList.transform(this.calendarActivities, item.id);

            let flPipe = new FilledDaysPipe();
            let filled = flPipe.transform(self.calendarActivities);
            this.fillCalendarEvents(filled);
          })
          .catch(err => err);
    }
  }

  getCalendarActivities(date) {
    let self = this;
    new Date();

    const firstDay = new Date(Date.UTC(date.getFullYear(), date.getMonth(), 1, 0, 0, 0));
    const lastDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 0);

    this.userService.getActivities(firstDay, lastDay, this.currentCalendar)
        .then(response => {
          let flPipe = new FilledDaysPipe();
          self.calendarActivities = response;
          let filled = flPipe.transform(self.calendarActivities);
          this.fillCalendarEvents(filled);
        })
        .catch(err => err);
  }

  getDateActivities(date: Date) {
    let self = this;
    let firstDay = new Date(date.getTime());
    let lastDay = new Date(date.getTime());

    firstDay.setHours(0, 0, 0, 0);
    lastDay.setHours(23, 59, 59, 999);

    this.activity.activity_date = date;
    this.activity.activity_day = date.getDay();
    this.activity.activity_time = date;

    this.userService.getActivities(firstDay, lastDay, this.currentCalendar)
        .then(response => {
          self.dateActivities = response;
        })
        .catch(err => err);
  }

  getTime(dt) {
    if (typeof dt === 'string') {
      return new Intl.DateTimeFormat('en-US',
          {hour: '2-digit', minute: '2-digit', hour12: false}).format(
          new Date(dt.replace(' ', 'T')));
    }
    if (typeof dt === 'object') {
      return new Intl.DateTimeFormat('en-US',
          {hour: '2-digit', minute: '2-digit', hour12: false}).format(dt);
    }
  }

  enterEditMode() {
    if (this.currentDate !== undefined) {
      let date = new Date(this.currentDate.date);
      let dt = new Date();
      let hours = dt.getHours();
      let min = dt.getMinutes();
      this.activity = {
        hour: hours.toString().length === 1 ? '0' + hours.toString() : hours.toString(),
        minute: min.toString().length === 1 ? '0' + min.toString() : min.toString(),
        repeat: 'not',
        activity_date: date,
        activity_day: date.getDay(),
        activity_time: date,
        activity_type: 'medicine',
        isActive: true,
        description: '',
        dt: new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)).toISOString(),
      };
      this.editMode = true;
    }
  }

  onSubmit(model, isValid) {
    let self = this;
    if (isValid) {
      this.activity.activity_time.setHours(parseInt(this.activity.hour),
          parseInt(this.activity.minute))
      this.userService.postActivity(this.currentCalendar, this.activity)
          .then((response) => {
            let key = self.activity.activity_date.toISOString();
            self.editMode = false;
            self.activity['id'] = true;
            // self.dateActivities.push(self.activity);

            let lastDay = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth() + 1, 0);

            let event = Object.assign({}, self.activity);

            self.calendarActivities.push(event);

            let date = new Date(self.currentDate.date);

            switch (event.repeat) {
              case 'not':
                break;
              case 'month':
                event = Object.assign({}, self.activity);
                date = new Date(date.setMonth(date.getMonth() + 1));
                event.dt = date.toISOString();
                self.calendarActivities.push(event);
                break;
              case 'week': case 'day':
                let step = event.repeat === 'week' ? 7 : 1;
                while (date <= lastDay) {
                  date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0));
                  date.setDate(date.getDate() + step);
                  event = Object.assign({}, self.activity);
                  event.dt = date.toISOString();
                  self.calendarActivities.push(event);
                }
                break;
            }

            let flPipe = new FilledDaysPipe();
            let filled = flPipe.transform(self.calendarActivities);
            self.fillCalendarEvents(filled);

            delete self.activity['id'];


          })
          .catch(err => err)
    }
  }

  monthChanged(event) {
    this.setMonthListTitle(event);
    this.getCalendarActivities(event);
  }

  changeCount(step, max, val) {
    let iVal = parseInt(val, 0);
    iVal = iVal ? iVal : 0;
    if (step > 0 && step + iVal > max) {
      iVal = 0;
    } else {
      if (step < 0 && step + iVal < 0) {
        iVal = max;
      } else {
        iVal = iVal + step;
      }
    }
    return iVal.toString().length === 1 ?
        '0' + iVal.toString() :
        iVal.toString();
  }

  dayClicked(event): void {
    if (this.currentDate) {
      delete this.currentDate.cssClass;
    }
    event.day.cssClass = 'cal-day-selected';
    this.currentDate = event.day;

    // this.getDateActivities(event.day.date);
  }
}
