import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filledDays'
})

export class FilledDaysPipe implements PipeTransform {

  transform(allDates) {
    let keys = [];
    return allDates.filter(item => {
      let key = item.dt;
      key = key.substr(0, key.indexOf('T'));
      if (typeof(keys[key]) === 'undefined' && item.id !== null) {
        keys[key] = true;
        return item.id !== null;
      }
      return false;
    });
  }
}
