import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AppSettings } from '../app.settings';

import { Profile } from '../models/profile';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  model = {
    firstName: '',
    surName: '',
    email: '',
    address: '',
    postalCode: '',
    city: '',
    country: '',
    phone: '',
    password: '',
    password_first: '',
    password_second: '',

  };
  products = [];
  serial = {};

  submitted = false;
  public phMask = ['+', /\d/, '(', /\d/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

  constructor(
    private userService: UserService,
    private router: Router,
    private appSetttings: AppSettings
  ) { }

  ngOnInit() {
    let self = this;

    if (this.appSetttings.loggedIn === false) {
      this.router.navigate(['signin']);
    }

    this.userService.getProfile().then((profile) => {
      self.model = profile;
    })
      .catch((err) => {

      });

    this.userService.getProducts().then((products) => {
      self.products = products;
    })
      .catch((err) => {

      });
  }

  onSubmit(data, isValid: boolean) {
    let self = this;
    if (isValid) {
      this.userService.patchProfile(data)
        .then(response => {
          data.submitted = true;
        })
        .catch((err) => {

        });
    }

  }

  changeProductsNames(data, isValid) {
    let self = this;
    if (isValid) {
      this.userService.patchProducts(data)
        .then(products => {
          self.products = products;
        })
        .catch((err) => {

        });
    }
  }

  addSerial(data, isValid) {
    let self = this;
    if (isValid) {
      this.userService.addProduct(data)
        .then(products => {
          self.products = products;
        })
        .catch((err) => {

        });
    }
  }
}
