import { Component, OnDestroy, OnInit, Inject, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PaymentFormComponent } from "../payment-form/payment-form.component"

import { UserService } from '../user/user.service';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css']
})

export class MembershipComponent implements OnInit, OnDestroy {

  state = 0;
  model = {};
  profile = {};
  order = {};

  public phMask = ['+', /\d/, '(', /\d/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

  constructor(
      private userService: UserService,
      @Inject(FormBuilder) private fb: FormBuilder
  ) {

    this.model = JSON.parse(sessionStorage.getItem('membership'));
  }

  ngOnInit() {
    this.state = sessionStorage.getItem('mstate') === null ? 0 : parseInt(sessionStorage.getItem('mstate'));
  }

  ngOnDestroy() {
    sessionStorage.removeItem('mstate');
    sessionStorage.removeItem('membership');
  }

  clickState() {
    if (this.state === 0) {
      this.state = 1;
      sessionStorage.setItem('mstate', this.state.toString());
    }
  }

 onSubmit(model, isValid: boolean) {
    if (isValid) {
      this.profile = model;
      if (this.state === 1) {
        this.state = 2;
        sessionStorage.setItem('mstate', this.state.toString());
      }
    }
  }

  onEdit() {
    if (this.state === 2) {
      this.state = 1;
      sessionStorage.setItem('mstate', this.state.toString());
    }
  }

  onPlaceOrder() {
    const self = this;

    if (this.state === 2) {
      this.userService.postRegister(this.profile).then((response) => {
        self.state = 3;
        sessionStorage.removeItem('mstate');
        sessionStorage.removeItem('membership');
        sessionStorage.setItem('order', JSON.stringify(response));
        self.order = response;
      }).catch((err) => {});
    }
  }

}
