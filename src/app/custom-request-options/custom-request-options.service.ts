// import { CookieService } from 'ngx-cookie';
import { Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomRequestOptionsService {

  constructor(
    // private cookieService: CookieService
  ) { }

  defaultRequestOptions() {
    const token = localStorage.getItem('token');

    if (token !== null) {
      return new RequestOptions({
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'JWT ' + JSON.parse(token),
        }),
      });
    }
    return new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
    });
  }
}
