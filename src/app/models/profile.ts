export class Profile {

  constructor(
    public id: number,
    public email: string,
    public verified: boolean,
    public firstName: string,
    public surName: string,
    public address: string,
    public postalCode: string,
    public city: string,
    public phone: string,
    public country: string,
    public membership: string,
  ) { }

}
