import { Component, OnInit } from '@angular/core';

import { UserService } from '../user/user.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  model = {email: 'mail@mail.com'};
  submitted = false;
  mail_send = false;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    const self = this;
    this.submitted = true;
    this.userService.postReset(this.model).then((profile) => {
      this.model = profile;
      self.mail_send = true;
    })
      .catch((err) => {

      });
  }
}
