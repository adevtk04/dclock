import { TestBed, async, inject } from '@angular/core/testing';

import { CheckRouteGuard } from './check-route.guard';

describe('CheckRouteGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckRouteGuard]
    });
  });

  it('should ...', inject([CheckRouteGuard], (guard: CheckRouteGuard) => {
    expect(guard).toBeTruthy();
  }));
});
