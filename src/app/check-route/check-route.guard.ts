import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';
import { AppSettings } from '../app.settings';

@Injectable()
export class CheckRouteGuard implements CanActivate {

  constructor(
      private appSettings: AppSettings,
      private userService: UserService,
      private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (['signin', 'restore'].includes(next.url[0].path)) {
      if (this.appSettings.token === null) {
        return true;
      }
      this.router.navigate(['/profile']);
      return false;
    }

    if (['profile', 'calendar'].includes(next.url[0].path)) {
      if (this.appSettings.token !== null) {
        return true;
      }
      this.router.navigate(['/signin']);
      return false;
    }

    if (next.url[0].path === 'restore') {
      return this.userService.getResetPassword(next.params.resetKey).then((response) => {
        return true;
      }).catch((err) => {
        this.router.navigate(['/']);
        return false;
      });
    }
    if (next.url[0].path === 'confirm') {
      return this.userService.getCheckEmail(next.params.confirmKey).then((response) => {
        return true;
      }).catch((err) => {
        this.router.navigate(['/']);
        return false;
      });
    }
  }
}
