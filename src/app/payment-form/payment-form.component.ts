import { Component, OnInit, NgZone } from '@angular/core';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.css']
})
export class PaymentFormComponent implements OnInit {

  payment_success: boolean = false;
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;

  order: any;

  message: string;

  constructor(
    private zone: NgZone,
    private userService: UserService
  ) {
    const self = this;
    let order = JSON.parse(sessionStorage.getItem('order'));
    this.userService.getOrder(order)
      .then(response => {
        self.order = response;
      })
      .catch(err => {});
  }

  ngOnInit() {
  }

  getToken() {
    this.message = 'Loading...';

    (<any>window).Stripe.card.createToken({
      number: this.cardNumber,
      exp_month: this.expiryMonth,
      exp_year: this.expiryYear,
      cvc: this.cvc
    }, (status: number, response: any) => {
      this.zone.run(() => {
        if (status === 200) {
          if (this.order !== null) {
            let order = this.order;
            let data = {
              user_id: order.User.id,
              order_id: order.id,
              amount: order.amount,
              token: response.id,
              created: response.created,
            }
            this.userService.postOrderPayment(data)
              .then(response => {
                this.message = response;
                this.payment_success = true;
              })
              .catch(err => {
                this.message = err;
              })
          }
        } else {
          this.message = response.error.message;
        }
      });
    });
  }
}
