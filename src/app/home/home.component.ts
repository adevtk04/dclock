import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  memberships = [];
  contact = {
    sender: '',
    email: '',
    message: ''
  };

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    const self = this;
    this.userService.getMembership().then((response) => {
      self.memberships = response;
    });
  }

  onSubmit() {
    const self = this;

    this.userService.postMessage(this.contact).then((response) => {
      self.contact =  {sender: '', email: '', message: ''};
    })
      .catch((err) => {

      });

  }

  membershipOnClick(membership) {
    sessionStorage.setItem('membership', JSON.stringify(membership));
    this.router.navigate(['membership']);
  }
}
