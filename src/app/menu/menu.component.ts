import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.settings';

import {UserAuthService} from '../user-auth/user-auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  route: string;
  isExpanded = false;
  mainMenu = true;
  // private router: Router;

  menu = [
    {link: 'one', title: 'A Day Clock', selected: true},
    {link: 'two', title: 'Product Detail'},
    {link: 'three', title: 'Solution'},
    {link: 'four', title: 'Membership'},
    {link: 'five', title: 'Contact'}
  ]

  constructor (
    private userAuthService: UserAuthService,
    public appSettings: AppSettings,
    private location: Location,
    private router: Router) {
    let self = this;


    router.events.subscribe((val) => {
      // if (self.appSettings.loggedIn === false) {
      //   self.router.navigate(['home']);
      // }
      if (location.path() != '') {
        self.route = location.path();
      } else {
        self.route = 'home'
      }
      if (['/profile', '/calendar'].indexOf(self.route) > -1) {
        this.mainMenu = false;
        self.menu = [
          {link: '/profile', title: 'Products and Account'},
          {link: '/calendar', title: 'Manage calendars'},
          {link: '/#five', title: 'Contact'}
        ]
      } else {
        if (this.mainMenu === false) {
          this.mainMenu = true;
          this.menu = [
            {link: 'one', title: 'A Day Clock', selected: true},
            {link: 'two', title: 'Product Detail'},
            {link: 'three', title: 'Solution'},
            {link: 'four', title: 'Membership'},
            {link: 'five', title: 'Contact'}
          ]
        }
      }
    });
  }

  select(item) {
    this.menu.forEach(item => item['selected'] = false);
    item['selected'] = true;
  }

  isActive(instruction: string): boolean {
    return this.router.isActive(instruction, true);
  }

  ngOnInit () {
  }

  logout () {
    var self = this;

    this.userAuthService.logout()
      .then((response) => {
        // if (self.appSettings.loggedIn === false) {
        //   self.router.navigate(['/home']);
        // }
      })
      .catch((err) => {

      });
  }
}
