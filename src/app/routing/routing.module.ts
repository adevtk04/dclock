import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CheckRouteGuard } from '../check-route/check-route.guard';

import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { ProfileComponent } from '../profile/profile.component';
import { ResetComponent } from '../reset/reset.component';
import { PasswordRestoreComponent } from '../password-restore/password-restore.component';
import { CalendarComponent } from '../calendar/calendar.component';
import { MembershipComponent } from '../membership/membership.component';
import { ConfirmComponent } from '../confirm/confirm.component';
import { PaymentFormComponent } from "../payment-form/payment-form.component"

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '', component: HomeComponent },
  { path: 'membership', component: MembershipComponent },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [CheckRouteGuard]
  },
  {
    path: 'calendar',
    component: CalendarComponent,
    canActivate: [CheckRouteGuard]
  },
  {
    path: 'signin',
    component: LoginComponent,
    canActivate: [CheckRouteGuard]
  },
  {
    path: 'restore',
    component: ResetComponent,
    canActivate: [CheckRouteGuard]
  },
  { path: 'confirm/:confirmKey',
    component: ConfirmComponent,
    canActivate: [CheckRouteGuard],
  },
  {
    path: 'restore/:resetKey',
    component: PasswordRestoreComponent,
    canActivate: [CheckRouteGuard]
  },
  {
    path: 'payment',
    component: PaymentFormComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [ RouterModule ]
})

export class RoutingModule { }
