import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Router} from '@angular/router'

import { CustomRequestOptionsService } from '../custom-request-options/custom-request-options.service';

import { AppSettings } from '../app.settings';

@Injectable()
export class UserAuthService {

  public token: string;

  constructor(
    private http: Http,
    private customRequestOptionsService: CustomRequestOptionsService,
    private appSettings: AppSettings,
    private router: Router,
  ) { }

  login(email, password) {
    const self = this;
    let data = {
      email,
      password
    };

    let url = this.appSettings.config.backend.url+this.appSettings.config.backend.login_uri;

    return this.http.post(url, data, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        let data = response.json();
        self.token = data.token;

        if (response.status === 200) {
          self.appSettings.loggedIn = true;
          self.appSettings.user = data;
          self.appSettings.token = data.token;
        } else {
          if (response.status !== 200) {
            self.appSettings.loggedIn = false;
            self.appSettings.user = null;
            self.appSettings.token = null;
          }
        }
        localStorage.setItem('loggedIn', JSON.stringify(self.appSettings.loggedIn));
        localStorage.setItem('user', JSON.stringify(self.appSettings.user));
        localStorage.setItem('token', JSON.stringify(self.appSettings.token));
        return response.json();
      })
      .catch((error: any) => {
        self.appSettings.loggedIn = false;
        self.appSettings.user = null;
        self.appSettings.token = null;
        localStorage.removeItem('loggedIn');
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        return Promise.reject(error.message || error);
      })
  }

  logout() {
    const self = this;
    let url = this.appSettings.config.backend.url+this.appSettings.config.backend.logout_uri;

    return this.http.get(url, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        let data = response.json();
        if (response.status === 200) {
          self.appSettings.loggedIn = false;
          self.appSettings.user = null;
          self.appSettings.token = null;
        } else {
          if (response.status !== 200) {
            self.appSettings.loggedIn = false;
            self.appSettings.user = null;
            self.appSettings.token = null;
          }
        }
        localStorage.removeItem('loggedIn');
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        self.router.navigateByUrl('/');
        return response.json();
      })
      .catch((error: any) => {
        self.appSettings.loggedIn = false;
        self.appSettings.user = null;
        self.appSettings.token = null;
        localStorage.removeItem('loggedIn');
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        return Promise.reject(error.message || error);
      })

  }

}
