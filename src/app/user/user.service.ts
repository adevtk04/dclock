import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Router} from '@angular/router'

import {CustomRequestOptionsService} from '../custom-request-options/custom-request-options.service';

import {AppSettings} from '../app.settings';

@Injectable()
export class UserService {

  public calendarDates;

  constructor (
    private http: Http,
    private customRequestOptionsService: CustomRequestOptionsService,
    private appSettings: AppSettings,
    private router: Router,) { }

  clearStorage () {
    this.appSettings.loggedIn = false;
    this.appSettings.user = null;
    this.appSettings.token = null;
    localStorage.removeItem('loggedIn');
    localStorage.removeItem('user');
    localStorage.removeItem('token');
  }

  getMembership () {
    const self = this;
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.membership_uri;

    return this.http.get(url)
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      });
  }

  patchProfile (data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.profile_uri;

    return this.http.patch(url, data,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      });
  }

  getProfile () {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.profile_uri;
    const self = this;

    return this.http.get(url,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {

        // self.getCalendars()
        //   .then((response) => {
        //     self.calendars = response;
        //     self.currentCalendar = self.calendars[0];
        //     self.getCalendarActivities(this.viewDate);
        //   })
        //   .catch((err) => {
        //     return err;
        //   });

        return response.json();


      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      });
  }

  postReset (data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.reset_uri;

    return this.http
      .post(url, data, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      });
  }

  getResetPassword (resetKey) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.reset_uri + '/' + resetKey;

    return this.http
      .get(url, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      });
  }

  getCheckEmail (confirmKey) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.confirm_uri + '/' + confirmKey;

    return this.http
      .get(url, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      });
  }

  postResetPassword (data, resetKey) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.reset_uri + '/' + resetKey;

    return this.http
      .post(url, data, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      });
  }

  postMessage (data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.contact_uri;

    return this.http
      .post(url, data, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      });
  }

  postRegister (data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.register_uri;

    return this.http
      .post(url, data, this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      });
  }

  getCalendars () {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.calendars_uri;

    return this.http.get(url,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      });
  }

  getActivities (from, to, calendar) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('from', from);
    params.set('to', to);

    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.activities_uri + '/' + calendar.id;

    let options = this.customRequestOptionsService.defaultRequestOptions();
    options.search = params;

    return this.http.get(url, options)
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      });
  }

  deleteActivity (calendar, data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.activities_uri + '/' + calendar.id + '/' + data.id;

    return this.http.delete(url,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      })
  }

  postActivity (calendar, data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.activities_uri + '/' + calendar.id;

    return this.http.post(url, data,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      })
  }

  getProducts () {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.products_uri;

    return this.http.get(url,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      })
  }

  patchProducts (data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.products_uri;

    return this.http.patch(url, data,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      })
  }

  addProduct (data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.products_uri;

    return this.http.post(url, data,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      })
  }

  getOrder(data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.order_uri + '/' + data.id;

    return this.http.get(url,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/');
          this.clearStorage();
          return error.status;
        }
        return Promise.reject(error.message || error);
      })
  }

  postOrderPayment(data) {
    const bk = this.appSettings.config.backend;
    const url = bk.url + bk.order_uri + '/' + data.order_id;

    return this.http.post(url, data,
      this.customRequestOptionsService.defaultRequestOptions())
      .toPromise()
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Promise.reject(error.message || error);
      })
  }
}
