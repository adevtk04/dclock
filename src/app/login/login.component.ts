import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';

import { AppSettings } from '../app.settings';

import { UserAuthService } from '../user-auth/user-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  submitted = false;

  model = {email: '', password: ''};

  constructor(
    private userAuthService: UserAuthService,
    private appSettings: AppSettings,
    private router: Router,
  ) { }

  ngOnInit() {
    if (this.appSettings.loggedIn) {
      this.router.navigate(['profile']);
    };
  }

  onSubmit() {
    const self = this;

    this.submitted = true;
    this.userAuthService.login(this.model.email, this.model.password)
      .then((response) => {
        if (self.appSettings.loggedIn) {
          self.router.navigate(['/profile']);
        };
      })
      .catch(err => err);
  }
}
