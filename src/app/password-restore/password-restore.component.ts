import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UserService } from '../user/user.service';

@Component({
  selector: 'app-password-restore',
  templateUrl: './password-restore.component.html',
  styleUrls: ['./password-restore.component.css']
})
export class PasswordRestoreComponent implements OnInit, OnDestroy {

  resetKey: string;
  private sub: any;
  model = {
    password_first: '',
    password_second: '',
  };
  submitted = false;

  constructor(
      private userService: UserService,
      private route: ActivatedRoute,
      private router: Router,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.resetKey = params['resetKey'];
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onSubmit(model, isValid: boolean) {
    if (isValid) {
      this.submitted = true;
      this.userService.postResetPassword(this.model, this.resetKey).then((response) => {
        this.model = response;
        this.router.navigate(['signin']);
      })
          .catch((err) => {

          });
    }
  }
}
