import { Adayclock.ComPage } from './app.po';

describe('adayclock.com App', () => {
  let page: Adayclock.ComPage;

  beforeEach(() => {
    page = new Adayclock.ComPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
