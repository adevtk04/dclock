/**
 * Created by hazarin on 07.06.17.
 */
var replace = require('replace-in-file');
var buildVersion = process.argv[2];
const options = {
  files: 'src/environments/environment.ts',
  replace: /{BUILD_VERSION}/g,
  with: buildVersion,
  allowEmptyPaths: false,
};

try {
  let changedFiles = replace.sync(options);
  console.log('Build version set: ' + buildVersion);
}
catch (error) {
  console.error('Error occurred:', error);
}
